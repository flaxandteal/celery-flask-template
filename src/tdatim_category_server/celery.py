from celery import Celery

app = Celery(
    'tdatim_category_server',
    broker='amqp://admin:pass@rabbitmq:5672',
    backend='rpc://',
    include=['tdatim_category_server.tasks']
)
