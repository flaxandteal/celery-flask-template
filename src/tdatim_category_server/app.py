from flask import jsonify
from flask import abort
from flask import request
from flask import Flask
import gettext
from celery import group
from . import tasks

app = Flask(__name__)
gettext.install('tdatim_category_server')

@app.route('/', methods=['POST'])
def get():
    data = request.get_json()

    if not data or type(data) is not list:
        abort(400, _("Must have a list of strings as posted JSON with text to categorize"))

    result = group([tasks.get_categories.s(x) for x in data]).apply_async()
    return jsonify(result.join())

if __name__ == "__main__":
    app.run(host='0.0.0.0')
