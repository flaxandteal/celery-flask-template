# Microservice Template

## Celery + Flask

To start, run:

    docker-compose up
    docker-compose scale worker=6

To execute tasks:

    docker-compose run --entrypoint python3 worker -m tdatim_category_server.run_tasks
