FROM python:3.6
ADD requirements.txt /requirements.txt
ADD ./src/ /app/
WORKDIR /app/
RUN useradd -u 1000 -ms /bin/sh tdatim
RUN pip3 install -r /requirements.txt
USER tdatim
ENTRYPOINT ["celery"]
CMD ["worker", "-A", "tdatim_category_server"]
