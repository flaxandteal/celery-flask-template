from setuptools import setup, find_packages

cmdclass = {}
try:
    from babel.messages import frontend as babel
    cmdclass.update({
        'compile_catalog': babel.compile_catalog,
        'extract_messages': babel.extract_messages,
        'init_catalog': babel.init_catalog,
        'update_catalog': babel.update_catalog,
    })
except ImportError as e:
    pass

try:
    from sphinx.setup_command import BuildDoc
    cmdclass['build_sphinx'] = BuildDoc
except ImportError as e:
    pass

name = 'tdatim_category_server'
version = '0.1'
release = '0.1.1'
setup(
    name=name,
    version=release,
    description='The Data Times: category server',
    url='https://github.com/thedatatimes/tdatim-category-server',
    author='The Data Times team',
    author_email='help@lintol.io',
    license='MIT',
    packages=find_packages('src'),
    package_dir={'': 'src'},
    classifiers=[
        'Development Status :: 3 - Alpha',
        'Intended Audience :: Developers',
        'License :: OSI Approved :: MIT License',
        'Programming Language :: Python :: 3.5'
    ],
    keywords='journalism dni data',
    setup_requires=['pytest-runner'],
    extras_require={
        'examples': ['shapely', 'piianalyzer', 'geojson_utils', 'geopandas'],
        'babel-commands': ['Babel'],
        'sphinx-commands': ['sphinx']
    },
    install_requires=[
        'celery',
        'requests',
        'flask',
        'gunicorn',
        'flask-babel'
    ],
    include_package_data=True,
    tests_require=[
        'pytest',
        'pytest-asyncio',
        'mock',
        'asynctest'
    ],
    cmdclass=cmdclass,
    command_options={
        'build_sphinx': {
            'project': ('setup.py', name),
            'version': ('setup.py', version),
            'release': ('setup.py', release)
        }
    }
)
